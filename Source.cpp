#include <iostream>
#include <iomanip>
#include <string>

int main() 
{
	std::string line("result");
	std::cout << line << "\n";
	std::cout << line.length() << "\n";
	std::cout << line.front() << "\n";
	std::cout << line.back() << "\n";
}